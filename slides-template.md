## GitLab
Everyone Can Contribute

<img data-src="img/logo-square.png" height="300" class="plain">

Note:
Here's my speaker note

---

<img data-src="img/logo-square.png" height="200" class="plain">

<h3>
    Section Header
</h3>

Note: 
Hello!


---
<!-- .slide: data-background-image="img/icon-pattern-background-purple.png" -->

<img data-src="img/stacked_wm_no_bg.png" class="plain" height="400">

A different background!

---
<!-- .slide: data-background-image="img/photo-background-working-4.png" -->
<!-- .slide: class="blacktext-slide" -->

And another background

With inverse text color!

---

## Some GitLab Colors

<span class="gitlab-orange-text">
	Orange
</span>

<span class="gitlab-yellow-text">
	Yellow
</span>

<span class="gitlab-purple-text">
	Purple
</span>

<span class="gitlab-red-text">
	Red
</span>

---

## Slides can go down too

+++

## Demo 2.1
<!-- .slide: data-background-color="#db3b21" -->
Background color instead of image

+++

## Demo 2.2

---

## GitLab fan icons included!

<img src="img/gitlabfan-smile.png" class="plain" height="450">

---

#### Bullets are the lifeblood of slides

- And we have them
- Such bullets
  - And content

---

#### Cool Table Animations

<table>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Local Repo
		</td>
		<td>
			Local copy of the upstream repo
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Remote <br/>
			<small>(Upstream repo)</small>
		</td>
		<td>
			Hosted repository on a shared server <small>(GitLab)</small>
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Untracked files
		</td>
		<td>
			New files that Git has not been told to track previously
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Working area
		</td>
		<td>
			Files that have been modified but not committed
		</td>
	</tr>
	<tr class="fragment fade-left">
		<td class="gitlab-orange-text">
			Staging area 
		</td>
		<td>
			Modified/Added files that are marked to go into the next commit
		</td>
	</tr>
</table>

---

## Building bulleted lists

- Git CLI setup
- Basic commands <!-- .element: class="fragment"  -->
- Workflow <!-- .element: class="fragment"  -->
- Merging & Rebasing <!-- .element: class="fragment"  -->
- Reverting & Reseting <!-- .element: class="fragment"  -->

---

## And pretty code colors

Inline

<pre><code class="javascript">function foo() { 
	let trucks = true;
}
</code></pre>
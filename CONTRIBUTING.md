## Contributing to GitLab Slide Template

The source of the GitLab Slide Template is [hosted on
GitLab.com](https://gitlab.com/gitlab-com/customer-success/slide-template/) and
contributions are welcome!

## Contributor license agreement

By submitting code as an individual you agree to the
[individual contributor license agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/individual_contributor_license_agreement.md).
By submitting code as an entity you agree to the
[corporate contributor license agreement](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/corporate_contributor_license_agreement.md).
